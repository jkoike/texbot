FROM registry.gitlab.com/jkoike/texbot:pre_build
COPY . .
RUN stack install && \
    rm -r ~/.stack /.stack-work
ENTRYPOINT texbot +RTS -T
